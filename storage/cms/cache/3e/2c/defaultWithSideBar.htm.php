<?php 
class Cms5d4cc8226140b900190640_52954c544839dd49b8f0724270a5154dClass extends Cms\Classes\LayoutCode
{
public function onStart()
{
    $this['search_query'] = get('q', $default = null);
    // current page url
    $parent = $this->page['apiBag']['staticPage']->getParent();
    $url = $this->page['apiBag']['staticPage']['viewBag']['url'];

    $currentPage = null;
    if($parent) {
        $children = $parent->getChildren();
        foreach( $children as $key => $page){
            if($page['viewBag']['url'] == $url) $currentPage = $key;
        }

        // previous page
        if ( array_key_exists($currentPage - 1, $children) ) {
            $this['prev_url'] = $children[$currentPage - 1]['viewBag']['url'];
            $this['prev_title'] = $children[$currentPage -1 ]['viewBag']['title'];
        }

        if ( array_key_exists($currentPage + 1, $children) ) {
            $this['next_url'] = $children[$currentPage + 1]['viewBag']['url'];
            $this['next_title'] = $children[$currentPage + 1]['viewBag']['title'];
        }
    // parent page
    $this['parent_title'] = $parent['viewBag']['title'];
    $this['parent_url'] = $parent['viewBag']['url'];
    }
}
}
