<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\_xampp\htdocs\coh/themes/art-mentoring-help/layouts/defaultWithSideBar.htm */
class __TwigTemplate_d7ce81d88346705e5ed327166231eaba2050d4444430c45e2a2bb95ae32fd187 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("head"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 2
        echo "
  <!-- Page Wrapper -->
  <div id=\"wrapper\">

   ";
        // line 6
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("sidebar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 7
        echo "   
    <!-- Content Wrapper -->
    <div id=\"content-wrapper\" class=\"d-flex flex-column\">

      <!-- Main Content -->
      <div id=\"content\">

       ";
        // line 14
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("navbar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 15
        echo "
        <!-- Begin Page Content -->
        <div class=\"container-fluid\">

          <!-- Page Heading 
          <div class=\"d-sm-flex align-items-center justify-content-between mb-4\">
            <h1 class=\"h3 mb-0 text-gray-800 breadcumbs\">";
        // line 21
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("staticBreadcrumbs"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h1>

          </div>-->

          

          <!-- Content Row -->

          <div class=\"row\">

            <!-- Area Chart -->
            <div class=\"col-xl-12 col-lg-12\">
              <div class=\"card shadow mb-4\">
                <!-- Card Header - Dropdown -->
                <div class=\"card-header py-3 d-flex flex-row align-items-center justify-content-between\">
                ";
        // line 36
        $context["parent"] = twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "getParent", [], "method", false, false, false, 36);
        // line 37
        echo "                <h6 class=\"m-0 font-weight-bold text-primary\"><i class=\"fas fa-fw fa-";
        if ((twig_get_attribute($this->env, $this->source,         // line 38
($context["parent"] ?? null), "title", [], "any", false, false, false, 38) == "Platform Overview")) {
            echo "desktop";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 39
($context["parent"] ?? null), "title", [], "any", false, false, false, 39) == "Program Settings")) {
            echo "calendar-day";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 40
($context["parent"] ?? null), "title", [], "any", false, false, false, 40) == "Resources")) {
            echo "file-pdf";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 41
($context["parent"] ?? null), "title", [], "any", false, false, false, 41) == "Applications")) {
            echo "check-square";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 42
($context["parent"] ?? null), "title", [], "any", false, false, false, 42) == "Groups")) {
            echo "users";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 43
($context["parent"] ?? null), "title", [], "any", false, false, false, 43) == "Participant Accounts")) {
            echo "user";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 44
($context["parent"] ?? null), "title", [], "any", false, false, false, 44) == "Communications")) {
            echo "satellite-dish";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 45
($context["parent"] ?? null), "title", [], "any", false, false, false, 45) == "Matching")) {
            echo "handshake";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 46
($context["parent"] ?? null), "title", [], "any", false, false, false, 46) == "Surveys")) {
            echo "poll";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 47
($context["parent"] ?? null), "title", [], "any", false, false, false, 47) == "Transfering Participants")) {
            echo "truck-moving";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 48
($context["parent"] ?? null), "title", [], "any", false, false, false, 48) == "Accessibility Settings")) {
            echo "universal-access";
        } else {
            // line 49
            echo "question";
        }
        echo "\"></i>
                   ";
        // line 50
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 50), "title", [], "any", false, false, false, 50), "html", null, true);
        echo " </h6>
                  
                </div>
                <div class=\"card-body\">
                <!-- Card Body -->
                ";
        // line 55
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("staticPage"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 56
        echo "                ";
        if ((twig_length_filter($this->env, ($context["prev_title"] ?? null)) > 0)) {
            // line 57
            echo "<div class=\"linkButton\"><a href=\"../../../coh";
            echo twig_escape_filter($this->env, ($context["prev_url"] ?? null), "html", null, true);
            echo "\" class=\"previous-link\"><span class=\"makeGrey\">&lt; &lt; Previous topic</span><span class=\"newLine\">";
            echo twig_escape_filter($this->env, ($context["prev_title"] ?? null), "html", null, true);
            echo "</span></a></div>
";
        }
        // line 59
        echo "
";
        // line 60
        if ((twig_length_filter($this->env, ($context["next_title"] ?? null)) > 0)) {
            // line 61
            echo "<div class=\"linkButton\"><a href=\"../../../coh";
            echo twig_escape_filter($this->env, ($context["next_url"] ?? null), "html", null, true);
            echo "\" class=\"next-link\"><span class=\"makeGrey\">Next topic &gt; &gt;</span><span class=\"newLine\">";
            echo twig_escape_filter($this->env, ($context["next_title"] ?? null), "html", null, true);
            echo "</span></a></div>
";
        }
        // line 63
        echo "                </div>
              </div>
            </div>

            
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      

";
        // line 75
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("foot"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/layouts/defaultWithSideBar.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 75,  169 => 63,  161 => 61,  159 => 60,  156 => 59,  148 => 57,  145 => 56,  141 => 55,  133 => 50,  128 => 49,  124 => 48,  121 => 47,  118 => 46,  115 => 45,  112 => 44,  109 => 43,  106 => 42,  103 => 41,  100 => 40,  97 => 39,  94 => 38,  92 => 37,  90 => 36,  70 => 21,  62 => 15,  58 => 14,  49 => 7,  45 => 6,  39 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% partial 'head' %}

  <!-- Page Wrapper -->
  <div id=\"wrapper\">

   {% partial 'sidebar' %}
   
    <!-- Content Wrapper -->
    <div id=\"content-wrapper\" class=\"d-flex flex-column\">

      <!-- Main Content -->
      <div id=\"content\">

       {% partial 'navbar' %}

        <!-- Begin Page Content -->
        <div class=\"container-fluid\">

          <!-- Page Heading 
          <div class=\"d-sm-flex align-items-center justify-content-between mb-4\">
            <h1 class=\"h3 mb-0 text-gray-800 breadcumbs\">{% component 'staticBreadcrumbs' %}</h1>

          </div>-->

          

          <!-- Content Row -->

          <div class=\"row\">

            <!-- Area Chart -->
            <div class=\"col-xl-12 col-lg-12\">
              <div class=\"card shadow mb-4\">
                <!-- Card Header - Dropdown -->
                <div class=\"card-header py-3 d-flex flex-row align-items-center justify-content-between\">
                {% set parent = page.getParent() %}
                <h6 class=\"m-0 font-weight-bold text-primary\"><i class=\"fas fa-fw fa-{% if 
                 parent.title == \"Platform Overview\" %}desktop{% elseif 
                 parent.title==\"Program Settings\" %}calendar-day{% elseif 
                 parent.title==\"Resources\" %}file-pdf{% elseif 
                 parent.title==\"Applications\" %}check-square{% elseif 
                 parent.title==\"Groups\" %}users{% elseif 
                 parent.title==\"Participant Accounts\" %}user{% elseif 
                 parent.title==\"Communications\" %}satellite-dish{% elseif 
                 parent.title==\"Matching\" %}handshake{% elseif 
                 parent.title==\"Surveys\" %}poll{% elseif 
                 parent.title==\"Transfering Participants\" %}truck-moving{% elseif
                 parent.title==\"Accessibility Settings\" %}universal-access{% else
                 %}question{% endif %}\"></i>
                   {{ this.page.title }} </h6>
                  
                </div>
                <div class=\"card-body\">
                <!-- Card Body -->
                {% component 'staticPage' %}
                {% if prev_title|length > 0 %}
<div class=\"linkButton\"><a href=\"../../../coh{{ prev_url }}\" class=\"previous-link\"><span class=\"makeGrey\">&lt; &lt; Previous topic</span><span class=\"newLine\">{{ prev_title }}</span></a></div>
{% endif%}

{% if next_title|length > 0 %}
<div class=\"linkButton\"><a href=\"../../../coh{{ next_url }}\" class=\"next-link\"><span class=\"makeGrey\">Next topic &gt; &gt;</span><span class=\"newLine\">{{ next_title }}</span></a></div>
{% endif%}
                </div>
              </div>
            </div>

            
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      

{% partial 'foot' %}", "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/layouts/defaultWithSideBar.htm", "");
    }
}
