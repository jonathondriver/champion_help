<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\_xampp\htdocs\coh/themes/art-mentoring-help/partials/forum-topics.htm */
class __TwigTemplate_3241ea52700cedf446e9d2653ac2011c24e116be33ffe9b0f3f00d03720a0c68 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"rainlab-forum\">

    
        ";
        // line 4
        $context["count"] = 0;
        // line 5
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["channels"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["channel"]) {
            // line 6
            echo "        ";
            $context["count"] = (($context["count"] ?? null) + 1);
            // line 7
            echo "        <li class=\"nav-item\">






            <a class=\"nav-link\" aria-controls=\"collapse";
            // line 14
            echo twig_escape_filter($this->env, ($context["count"] ?? null), "html", null, true);
            echo "\"  href=\"#\" data-toggle=\"collapse\" data-target=\"#collapse";
            echo twig_escape_filter($this->env, ($context["count"] ?? null), "html", null, true);
            echo "\" aria-expanded=\"";
            echo (((twig_get_attribute($this->env, $this->source, $context["channel"], "isActive", [], "any", false, false, false, 14) || twig_get_attribute($this->env, $this->source, $context["channel"], "isChildActive", [], "any", false, false, false, 14))) ? ("true") : ("false"));
            echo "\" aria-controls=\"collapse";
            echo twig_escape_filter($this->env, ($context["count"] ?? null), "html", null, true);
            echo "\"><i class=\"fas fa-fw fa-";
            if ((($context["count"] ?? null) == 1)) {
                echo "desktop";
            } elseif ((($context["count"] ?? null) == 2)) {
                echo "calendar-day";
            } elseif ((            // line 15
($context["count"] ?? null) == 3)) {
                echo "file-pdf";
            } elseif ((($context["count"] ?? null) == 4)) {
                echo "check-square";
            } elseif ((($context["count"] ?? null) == 5)) {
                echo "users";
            } elseif ((($context["count"] ?? null) == 6)) {
                echo "user";
            } elseif ((($context["count"] ?? null) == 7)) {
                // line 16
                echo "satellite-dish";
            } elseif ((($context["count"] ?? null) == 8)) {
                echo "handshake";
            } elseif ((($context["count"] ?? null) == 9)) {
                echo "poll";
            } elseif ((($context["count"] ?? null) == 10)) {
                echo "truck-moving";
            } elseif ((($context["count"] ?? null) == 11)) {
                // line 17
                echo "question";
            } elseif ((($context["count"] ?? null) == 2)) {
            }
            echo "\"></i>
                    <span>";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["channel"], "title", [], "any", false, false, false, 18), "html", null, true);
            echo "</span>
                  </a>
              

            ";
            // line 22
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["channel"], "relations", [], "any", false, false, false, 22), "children", [], "any", false, false, false, 22)) {
                // line 23
                echo "            <div id=\"collapse";
                echo twig_escape_filter($this->env, ($context["count"] ?? null), "html", null, true);
                echo "\" class=\"collapse ";
                echo (((twig_get_attribute($this->env, $this->source, $context["channel"], "isActive", [], "any", false, false, false, 23) || twig_get_attribute($this->env, $this->source, $context["channel"], "isChildActive", [], "any", false, false, false, 23))) ? ("show") : (""));
                echo "\" aria-labelledby=\"heading";
                echo twig_escape_filter($this->env, ($context["count"] ?? null), "html", null, true);
                echo "\" data-parent=\"#accordionSidebar\" style=\"\">
                    <div class=\"bg-white py-2 collapse-inner rounded\">
                   
                ";
                // line 26
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["channel"], "children", [], "any", false, false, false, 26));
                foreach ($context['_seq'] as $context["_key"] => $context["channel"]) {
                    // line 27
                    echo "                <a class=\"collapse-item\" href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["channel"], "url", [], "any", false, false, false, 27), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["channel"], "title", [], "any", false, false, false, 27), "html", null, true);
                    echo "</a>
                                
                            ";
                    // line 29
                    if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["channel"], "children", [], "any", false, false, false, 29))) {
                        // line 30
                        echo "                                ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["channel"], "children", [], "any", false, false, false, 30));
                        foreach ($context['_seq'] as $context["_key"] => $context["channel"]) {
                            // line 31
                            echo "                                <a class=\"collapse-item\" href=\"";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["channel"], "url", [], "any", false, false, false, 31), "html", null, true);
                            echo "\"> - ";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["channel"], "title", [], "any", false, false, false, 31), "html", null, true);
                            echo "</a>
                                        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['channel'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 33
                        echo "                               
                            ";
                    }
                    // line 35
                    echo "                       
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['channel'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 37
                echo "            </li>  
            ";
            } else {
                // line 39
                echo "                <tr>
                    <td colspan=\"100\"><p>There are no channels to show.</p></td>
                </tr>
            ";
            }
            // line 43
            echo "
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['channel'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "    
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/partials/forum-topics.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 45,  168 => 43,  162 => 39,  158 => 37,  151 => 35,  147 => 33,  136 => 31,  131 => 30,  129 => 29,  121 => 27,  117 => 26,  106 => 23,  104 => 22,  97 => 18,  91 => 17,  82 => 16,  72 => 15,  59 => 14,  50 => 7,  47 => 6,  42 => 5,  40 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"rainlab-forum\">

    
        {% set count = 0 %}
        {% for channel in channels %}
        {% set count = count + 1 %}
        <li class=\"nav-item\">






            <a class=\"nav-link\" aria-controls=\"collapse{{ count }}\"  href=\"#\" data-toggle=\"collapse\" data-target=\"#collapse{{ count }}\" aria-expanded=\"{{ channel.isActive or channel.isChildActive ? 'true' : 'false' }}\" aria-controls=\"collapse{{ count }}\"><i class=\"fas fa-fw fa-{% if count == 1 %}desktop{% elseif count==2 %}calendar-day{% elseif
                 count==3 %}file-pdf{% elseif count==4 %}check-square{% elseif count==5 %}users{% elseif count==6 %}user{% elseif count==7
                     %}satellite-dish{% elseif count==8 %}handshake{% elseif count==9 %}poll{% elseif count==10 %}truck-moving{% elseif count==11
                         %}question{% elseif count==2 %}{% endif %}\"></i>
                    <span>{{ channel.title }}</span>
                  </a>
              

            {% if channel.relations.children %}
            <div id=\"collapse{{ count }}\" class=\"collapse {{ channel.isActive or channel.isChildActive ? 'show' : '' }}\" aria-labelledby=\"heading{{ count }}\" data-parent=\"#accordionSidebar\" style=\"\">
                    <div class=\"bg-white py-2 collapse-inner rounded\">
                   
                {% for channel in channel.children %}
                <a class=\"collapse-item\" href=\"{{ channel.url }}\">{{ channel.title }}</a>
                                
                            {% if channel.children|length %}
                                {% for channel in channel.children %}
                                <a class=\"collapse-item\" href=\"{{ channel.url }}\"> - {{ channel.title }}</a>
                                        {% endfor %}
                               
                            {% endif %}
                       
                {% endfor %}
            </li>  
            {% else %}
                <tr>
                    <td colspan=\"100\"><p>There are no channels to show.</p></td>
                </tr>
            {% endif %}

        {% endfor %}
    
</div>", "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/partials/forum-topics.htm", "");
    }
}
