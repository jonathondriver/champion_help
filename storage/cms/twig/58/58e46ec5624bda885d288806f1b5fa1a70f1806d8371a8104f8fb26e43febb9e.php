<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\_xampp\htdocs\coh/themes/art-mentoring-help/partials/sidebar.htm */
class __TwigTemplate_e7b21c6f37f3cb4949a4816f8bd7e263444a87e467f55063407d4d8ddfad2bde extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!-- Sidebar -->
    <ul class=\"navbar-nav bg-gradient-primary sidebar sidebar-dark accordion\" id=\"accordionSidebar\">

      <!-- Sidebar - Brand -->
      <a class=\"sidebar-brand d-flex align-items-center justify-content-center\" href=\"../../coh/\">
        <div class=\"sidebar-brand-icon\">
          <img src=\"../../../coh/storage/app/media/AoM-bullet.png\" style=\"max-width: 100%;\" />
        </div>
        <div class=\"sidebar-brand-text mx-3\">Art of Mentoring </div>
      </a>

      <!-- Divider -->
      

      <!-- Nav Item - Dashboard -->


      <!-- Divider -->
      <hr class=\"sidebar-divider my-0\">

      <!-- Heading -->
      <div class=\"sidebar-heading\">
    <h5>    Champion Guides </h5>
    
    
      </div>
      <hr class=\"sidebar-divider my-0\">
     <!--<div class=\"sidebar-heading\"> <h5><a href=\"../../../coh/forum\"> FAQs FORUM </a></h5></div>
      <hr class=\"sidebar-divider my-0\">-->
      <p style=\"text-align:center;\">Topics:</p>
";
        // line 31
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['items'] = twig_get_attribute($this->env, $this->source, ($context["staticMenu"] ?? null), "menuItems", [], "any", false, false, false, 31)        ;
        $context['__cms_partial_params']['class'] = "nav navbar-nav"        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("menu-items"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        echo " 
      <!-- Nav Item - Pages Collapse Menu -->
      

      <!-- Divider -->
      <hr class=\"sidebar-divider d-none d-md-block\">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class=\"text-center d-none d-md-inline\">
        <button class=\"rounded-circle border-0\" id=\"sidebarToggle\"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->";
    }

    public function getTemplateName()
    {
        return "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/partials/sidebar.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 31,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- Sidebar -->
    <ul class=\"navbar-nav bg-gradient-primary sidebar sidebar-dark accordion\" id=\"accordionSidebar\">

      <!-- Sidebar - Brand -->
      <a class=\"sidebar-brand d-flex align-items-center justify-content-center\" href=\"../../coh/\">
        <div class=\"sidebar-brand-icon\">
          <img src=\"../../../coh/storage/app/media/AoM-bullet.png\" style=\"max-width: 100%;\" />
        </div>
        <div class=\"sidebar-brand-text mx-3\">Art of Mentoring </div>
      </a>

      <!-- Divider -->
      

      <!-- Nav Item - Dashboard -->


      <!-- Divider -->
      <hr class=\"sidebar-divider my-0\">

      <!-- Heading -->
      <div class=\"sidebar-heading\">
    <h5>    Champion Guides </h5>
    
    
      </div>
      <hr class=\"sidebar-divider my-0\">
     <!--<div class=\"sidebar-heading\"> <h5><a href=\"../../../coh/forum\"> FAQs FORUM </a></h5></div>
      <hr class=\"sidebar-divider my-0\">-->
      <p style=\"text-align:center;\">Topics:</p>
{% partial 'menu-items' items=staticMenu.menuItems class='nav navbar-nav' %} 
      <!-- Nav Item - Pages Collapse Menu -->
      

      <!-- Divider -->
      <hr class=\"sidebar-divider d-none d-md-block\">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class=\"text-center d-none d-md-inline\">
        <button class=\"rounded-circle border-0\" id=\"sidebarToggle\"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->", "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/partials/sidebar.htm", "");
    }
}
