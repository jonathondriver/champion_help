<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\_xampp\htdocs\coh/themes/art-mentoring-help/layouts/Forum.htm */
class __TwigTemplate_d7fe41e255b0114c10630e756a06dd63dcae850a22598d514218e793c806b854 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("head"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 2
        echo "
  <!-- Page Wrapper -->
  <div id=\"wrapper\">

    <!-- Sidebar -->
    <ul class=\"navbar-nav bg-gradient-primary sidebar sidebar-dark accordion\" id=\"accordionSidebar\">

      <!-- Sidebar - Brand -->
      <a class=\"sidebar-brand d-flex align-items-center justify-content-center\" href=\"index.html\">
        <div class=\"sidebar-brand-icon\">
          <img src=\"../../../coh/storage/app/media/Logo-mentoring-x-large.png\" style=\"max-width: 100%;\" />
        </div>
        <div class=\"sidebar-brand-text mx-3\">Art of Mentoring </div>
      </a>

      <!-- Divider -->
      

      <!-- Nav Item - Dashboard -->


    <!-- Divider -->
      <hr class=\"sidebar-divider my-0\">

      <!-- Heading -->
      <div class=\"sidebar-heading\">
    <h5> <a href=\"../../../coh/platform-overview/platform\">   Champion Guides </a></h5>
    
    
      </div>
      <hr class=\"sidebar-divider my-0\">
     <div class=\"sidebar-heading\"> <h5> FAQs FORUM </h5></div>
      <hr class=\"sidebar-divider my-0\">
      <p style=\"text-align:center;\">Topics:</p>
 ";
        // line 36
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("forum-topics"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 37
        echo "      <!-- Nav Item - Pages Collapse Menu -->
      <!-- Divider -->
      <hr class=\"sidebar-divider d-none d-md-block\">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class=\"text-center d-none d-md-inline\">
        <button class=\"rounded-circle border-0\" id=\"sidebarToggle\"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id=\"content-wrapper\" class=\"d-flex flex-column\">

      <!-- Main Content -->
      <div id=\"content\">

        <!-- Topbar -->
        <nav class=\"navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow\">

          <!-- Sidebar Toggle (Topbar) -->
          <button id=\"sidebarToggleTop\" class=\"btn btn-link d-md-none rounded-circle mr-3\">
            <i class=\"fa fa-bars\"></i>
          </button>

          <!-- Topbar Search -->
          <div class=\"d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search\">
            <div class=\"input-group\">
              <input type=\"text\" class=\"form-control bg-light border-0 small\" placeholder=\"Search for...\" aria-label=\"Search\" aria-describedby=\"basic-addon2\">";
        // line 66
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("searchInput"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 67
        echo "              <div class=\"input-group-append\">
                <button class=\"btn btn-primary\" type=\"button\">
                  <i class=\"fas fa-search fa-sm\"></i>
                </button>
              </div>
            </div>
          </div>

          <!-- Topbar Navbar -->
          <ul class=\"navbar-nav ml-auto\">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class=\"nav-item dropdown no-arrow d-sm-none\">
              <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"searchDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                <i class=\"fas fa-search fa-fw\"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class=\"dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in\" aria-labelledby=\"searchDropdown\">
                <form class=\"form-inline mr-auto w-100 navbar-search\">
                  <div class=\"input-group\">
                    <input type=\"text\" class=\"form-control bg-light border-0 small\" placeholder=\"Search for...\" aria-label=\"Search\" aria-describedby=\"basic-addon2\">
                    <div class=\"input-group-append\">
                      <button class=\"btn btn-primary\" type=\"button\">
                        <i class=\"fas fa-search fa-sm\"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

           


            <div class=\"topbar-divider d-none d-sm-block\"></div>

            <!-- Nav Item - User Information -->
            <li class=\"nav-item dropdown no-arrow\">
              <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"userDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                ";
        // line 106
        if (($context["user"] ?? null)) {
            // line 107
            echo "                                
                            
                <span class=\"mr-2 d-none d-lg-inline text-gray-600 small\">Some User</span>
                <img class=\"img-profile rounded-circle\" src=\"https://source.unsplash.com/QAB-WJcbgJk/60x60\">
                ";
        } else {
            // line 112
            echo "                <span class=\"mr-2 d-none d-lg-inline text-gray-600 small\">Login</span>
                
                ";
        }
        // line 115
        echo "              </a>
              <!-- Dropdown - User Information -->
              <div class=\"dropdown-menu dropdown-menu-right shadow animated--grow-in\" aria-labelledby=\"userDropdown\">
                <a class=\"dropdown-item\" href=\"\"";
        // line 118
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("account");
        echo "\"\">
                  <i class=\"fas fa-user fa-sm fa-fw mr-2 text-gray-400\"></i>
                  Profile
                </a>
                <a class=\"dropdown-item\" href=\"#\">
                  <i class=\"fas fa-list fa-sm fa-fw mr-2 text-gray-400\"></i>
                  Activity Log
                </a>
                <div class=\"dropdown-divider\"></div>
                <a class=\"dropdown-item\" href=\"#\" data-toggle=\"modal\" data-target=\"#logoutModal\">
                  <i class=\"fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400\"></i>
                  
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class=\"container-fluid\">

          <!-- Page Heading 
          <div class=\"d-sm-flex align-items-center justify-content-between mb-4\">
            <h1 class=\"h3 mb-0 text-gray-800 breadcumbs\">";
        // line 145
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("staticBreadcrumbs"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h1>

          </div>-->

          

          <!-- Content Row -->

          <div class=\"row\">

            <!-- Area Chart -->
            <div class=\"col-xl-12 col-lg-12\">
              <div class=\"card shadow mb-4\">
                <!-- Card Header - Dropdown -->
                <div class=\"card-header py-3 d-flex flex-row align-items-center justify-content-between\">
                  <h6 class=\"m-0 font-weight-bold text-primary\"> ";
        // line 160
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 160), "title", [], "any", false, false, false, 160), "html", null, true);
        echo " </h6>
                  
                </div>
                <div class=\"card-body\">
                <!-- Card Body -->
                ";
        // line 165
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 166
        echo "                ";
        if ((twig_length_filter($this->env, ($context["prev_title"] ?? null)) > 0)) {
            // line 167
            echo "<div class=\"linkButton\"><a href=\"";
            echo twig_escape_filter($this->env, ($context["prev_url"] ?? null), "html", null, true);
            echo "\" class=\"previous-link\"><span class=\"makeGrey\">&lt; &lt; Previous topic</span><span class=\"newLine\">";
            echo twig_escape_filter($this->env, ($context["prev_title"] ?? null), "html", null, true);
            echo "</span></a></div>
";
        }
        // line 169
        echo "
";
        // line 170
        if ((twig_length_filter($this->env, ($context["next_title"] ?? null)) > 0)) {
            // line 171
            echo "<div class=\"linkButton\"><a href=\"";
            echo twig_escape_filter($this->env, ($context["next_url"] ?? null), "html", null, true);
            echo "\" class=\"next-link\"><span class=\"makeGrey\">Next topic &gt; &gt;</span><span class=\"newLine\">";
            echo twig_escape_filter($this->env, ($context["next_title"] ?? null), "html", null, true);
            echo "</span></a></div>
";
        }
        // line 173
        echo "                </div>
              </div>
            </div>

            
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class=\"sticky-footer bg-white\">
        <div class=\"container my-auto\">
          <div class=\"copyright text-center my-auto\">
            <span>Copyright &copy; Art of Mentoring 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class=\"scroll-to-top rounded\" href=\"#page-top\">
    <i class=\"fas fa-angle-up\"></i>
  </a>

  <!-- Logout Modal-->
  <div class=\"modal fade\" id=\"logoutModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\" role=\"document\">
      <div class=\"modal-content\">
        <div class=\"modal-header\">
          <h5 class=\"modal-title\" id=\"exampleModalLabel\">Ready to Leave?</h5>
          <button class=\"close\" type=\"button\" data-dismiss=\"modal\" aria-label=\"Close\">
            <span aria-hidden=\"true\">×</span>
          </button>
        </div>
        <div class=\"modal-body\">Select \"Logout\" below if you are ready to end your current session.</div>
        <div class=\"modal-footer\">
          <button class=\"btn btn-secondary\" type=\"button\" data-dismiss=\"modal\">Cancel</button>
          <a class=\"btn btn-primary\" href=\"login.html\">Logout</a>
        </div>
      </div>
    </div>
  </div>

";
        // line 223
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("foot"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/layouts/Forum.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  310 => 223,  258 => 173,  250 => 171,  248 => 170,  245 => 169,  237 => 167,  234 => 166,  232 => 165,  224 => 160,  204 => 145,  174 => 118,  169 => 115,  164 => 112,  157 => 107,  155 => 106,  114 => 67,  110 => 66,  79 => 37,  75 => 36,  39 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% partial 'head' %}

  <!-- Page Wrapper -->
  <div id=\"wrapper\">

    <!-- Sidebar -->
    <ul class=\"navbar-nav bg-gradient-primary sidebar sidebar-dark accordion\" id=\"accordionSidebar\">

      <!-- Sidebar - Brand -->
      <a class=\"sidebar-brand d-flex align-items-center justify-content-center\" href=\"index.html\">
        <div class=\"sidebar-brand-icon\">
          <img src=\"../../../coh/storage/app/media/Logo-mentoring-x-large.png\" style=\"max-width: 100%;\" />
        </div>
        <div class=\"sidebar-brand-text mx-3\">Art of Mentoring </div>
      </a>

      <!-- Divider -->
      

      <!-- Nav Item - Dashboard -->


    <!-- Divider -->
      <hr class=\"sidebar-divider my-0\">

      <!-- Heading -->
      <div class=\"sidebar-heading\">
    <h5> <a href=\"../../../coh/platform-overview/platform\">   Champion Guides </a></h5>
    
    
      </div>
      <hr class=\"sidebar-divider my-0\">
     <div class=\"sidebar-heading\"> <h5> FAQs FORUM </h5></div>
      <hr class=\"sidebar-divider my-0\">
      <p style=\"text-align:center;\">Topics:</p>
 {% partial 'forum-topics' %}
      <!-- Nav Item - Pages Collapse Menu -->
      <!-- Divider -->
      <hr class=\"sidebar-divider d-none d-md-block\">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class=\"text-center d-none d-md-inline\">
        <button class=\"rounded-circle border-0\" id=\"sidebarToggle\"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id=\"content-wrapper\" class=\"d-flex flex-column\">

      <!-- Main Content -->
      <div id=\"content\">

        <!-- Topbar -->
        <nav class=\"navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow\">

          <!-- Sidebar Toggle (Topbar) -->
          <button id=\"sidebarToggleTop\" class=\"btn btn-link d-md-none rounded-circle mr-3\">
            <i class=\"fa fa-bars\"></i>
          </button>

          <!-- Topbar Search -->
          <div class=\"d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search\">
            <div class=\"input-group\">
              <input type=\"text\" class=\"form-control bg-light border-0 small\" placeholder=\"Search for...\" aria-label=\"Search\" aria-describedby=\"basic-addon2\">{% component 'searchInput' %}
              <div class=\"input-group-append\">
                <button class=\"btn btn-primary\" type=\"button\">
                  <i class=\"fas fa-search fa-sm\"></i>
                </button>
              </div>
            </div>
          </div>

          <!-- Topbar Navbar -->
          <ul class=\"navbar-nav ml-auto\">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class=\"nav-item dropdown no-arrow d-sm-none\">
              <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"searchDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                <i class=\"fas fa-search fa-fw\"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class=\"dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in\" aria-labelledby=\"searchDropdown\">
                <form class=\"form-inline mr-auto w-100 navbar-search\">
                  <div class=\"input-group\">
                    <input type=\"text\" class=\"form-control bg-light border-0 small\" placeholder=\"Search for...\" aria-label=\"Search\" aria-describedby=\"basic-addon2\">
                    <div class=\"input-group-append\">
                      <button class=\"btn btn-primary\" type=\"button\">
                        <i class=\"fas fa-search fa-sm\"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

           


            <div class=\"topbar-divider d-none d-sm-block\"></div>

            <!-- Nav Item - User Information -->
            <li class=\"nav-item dropdown no-arrow\">
              <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"userDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                {% if user %}
                                
                            
                <span class=\"mr-2 d-none d-lg-inline text-gray-600 small\">Some User</span>
                <img class=\"img-profile rounded-circle\" src=\"https://source.unsplash.com/QAB-WJcbgJk/60x60\">
                {% else %}
                <span class=\"mr-2 d-none d-lg-inline text-gray-600 small\">Login</span>
                
                {% endif %}
              </a>
              <!-- Dropdown - User Information -->
              <div class=\"dropdown-menu dropdown-menu-right shadow animated--grow-in\" aria-labelledby=\"userDropdown\">
                <a class=\"dropdown-item\" href=\"\"{{ 'account'|page }}\"\">
                  <i class=\"fas fa-user fa-sm fa-fw mr-2 text-gray-400\"></i>
                  Profile
                </a>
                <a class=\"dropdown-item\" href=\"#\">
                  <i class=\"fas fa-list fa-sm fa-fw mr-2 text-gray-400\"></i>
                  Activity Log
                </a>
                <div class=\"dropdown-divider\"></div>
                <a class=\"dropdown-item\" href=\"#\" data-toggle=\"modal\" data-target=\"#logoutModal\">
                  <i class=\"fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400\"></i>
                  
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class=\"container-fluid\">

          <!-- Page Heading 
          <div class=\"d-sm-flex align-items-center justify-content-between mb-4\">
            <h1 class=\"h3 mb-0 text-gray-800 breadcumbs\">{% component 'staticBreadcrumbs' %}</h1>

          </div>-->

          

          <!-- Content Row -->

          <div class=\"row\">

            <!-- Area Chart -->
            <div class=\"col-xl-12 col-lg-12\">
              <div class=\"card shadow mb-4\">
                <!-- Card Header - Dropdown -->
                <div class=\"card-header py-3 d-flex flex-row align-items-center justify-content-between\">
                  <h6 class=\"m-0 font-weight-bold text-primary\"> {{ this.page.title }} </h6>
                  
                </div>
                <div class=\"card-body\">
                <!-- Card Body -->
                {% page %}
                {% if prev_title|length > 0 %}
<div class=\"linkButton\"><a href=\"{{ prev_url }}\" class=\"previous-link\"><span class=\"makeGrey\">&lt; &lt; Previous topic</span><span class=\"newLine\">{{ prev_title }}</span></a></div>
{% endif%}

{% if next_title|length > 0 %}
<div class=\"linkButton\"><a href=\"{{ next_url }}\" class=\"next-link\"><span class=\"makeGrey\">Next topic &gt; &gt;</span><span class=\"newLine\">{{ next_title }}</span></a></div>
{% endif%}
                </div>
              </div>
            </div>

            
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class=\"sticky-footer bg-white\">
        <div class=\"container my-auto\">
          <div class=\"copyright text-center my-auto\">
            <span>Copyright &copy; Art of Mentoring 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class=\"scroll-to-top rounded\" href=\"#page-top\">
    <i class=\"fas fa-angle-up\"></i>
  </a>

  <!-- Logout Modal-->
  <div class=\"modal fade\" id=\"logoutModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\" role=\"document\">
      <div class=\"modal-content\">
        <div class=\"modal-header\">
          <h5 class=\"modal-title\" id=\"exampleModalLabel\">Ready to Leave?</h5>
          <button class=\"close\" type=\"button\" data-dismiss=\"modal\" aria-label=\"Close\">
            <span aria-hidden=\"true\">×</span>
          </button>
        </div>
        <div class=\"modal-body\">Select \"Logout\" below if you are ready to end your current session.</div>
        <div class=\"modal-footer\">
          <button class=\"btn btn-secondary\" type=\"button\" data-dismiss=\"modal\">Cancel</button>
          <a class=\"btn btn-primary\" href=\"login.html\">Logout</a>
        </div>
      </div>
    </div>
  </div>

{% partial 'foot' %}", "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/layouts/Forum.htm", "");
    }
}
