<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\_xampp\htdocs\coh/themes/art-mentoring-help/pages/search-results.htm */
class __TwigTemplate_ba574d96b4f029f946a8e534f396c9d303673443062191b34caac02b9f43bd3d extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("head"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 2
        echo "
  <!-- Page Wrapper -->
  <div id=\"wrapper\">

   ";
        // line 6
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("sidebar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 7
        echo "
    <!-- Content Wrapper -->
    <div id=\"content-wrapper\" class=\"d-flex flex-column\">

      <!-- Main Content -->
      <div id=\"content\">
";
        // line 13
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("navbar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 14
        echo "
        <!-- Begin Page Content -->
        <div class=\"container-fluid\">

<h1>Search results</h1>
";
        // line 19
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("searchResults"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 20
        echo "                </div>
              </div>
            </div>

            
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      

";
        // line 32
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("foot"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/pages/search-results.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 32,  72 => 20,  68 => 19,  61 => 14,  57 => 13,  49 => 7,  45 => 6,  39 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% partial 'head' %}

  <!-- Page Wrapper -->
  <div id=\"wrapper\">

   {% partial 'sidebar' %}

    <!-- Content Wrapper -->
    <div id=\"content-wrapper\" class=\"d-flex flex-column\">

      <!-- Main Content -->
      <div id=\"content\">
{% partial 'navbar' %}

        <!-- Begin Page Content -->
        <div class=\"container-fluid\">

<h1>Search results</h1>
{% component 'searchResults' %}
                </div>
              </div>
            </div>

            
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      

{% partial 'foot' %}", "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/pages/search-results.htm", "");
    }
}
