<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\_xampp\htdocs\coh/themes/art-mentoring-help/layouts/topicIndex.htm */
class __TwigTemplate_712f78dbf7c158deaea46e71eca9b732acc264c88865e10e03811e3179df63ed extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("head"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 2
        echo "
  <!-- Page Wrapper -->
  <div id=\"wrapper\">

   ";
        // line 6
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("sidebar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 7
        echo "
    <!-- Content Wrapper -->
    <div id=\"content-wrapper\" class=\"d-flex flex-column\">

      <!-- Main Content -->
      <div id=\"content\">
";
        // line 13
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("navbar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 14
        echo "
        <!-- Begin Page Content -->
        <div class=\"container-fluid\">

          <!-- Page Heading 
          <div class=\"d-sm-flex align-items-center justify-content-between mb-4\">
            <h1 class=\"h3 mb-0 text-gray-800 breadcumbs\">";
        // line 20
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("staticBreadcrumbs"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</h1>

          </div>-->

          

          <!-- Content Row -->

          <div class=\"row\">

            <!-- Area Chart -->
            <div class=\"col-xl-12 col-lg-12\">
              <div class=\"card shadow mb-4\">
                <!-- Card Header - Dropdown -->
                <div class=\"card-header py-3 d-flex flex-row align-items-center justify-content-between\">
                   <div class=\"row\" style=\"    width: 100%;\">
                <div class=\"col-md-12 makeMid\">
                 <i class=\"fas fa-fw fa-";
        // line 37
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 38
($context["this"] ?? null), "page", [], "any", false, false, false, 38), "title", [], "any", false, false, false, 38) == "Platform Overview")) {
            echo "desktop";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 39
($context["this"] ?? null), "page", [], "any", false, false, false, 39), "title", [], "any", false, false, false, 39) == "Program Settings")) {
            echo "calendar-day";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 40
($context["this"] ?? null), "page", [], "any", false, false, false, 40), "title", [], "any", false, false, false, 40) == "Resources")) {
            echo "file-pdf";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 41
($context["this"] ?? null), "page", [], "any", false, false, false, 41), "title", [], "any", false, false, false, 41) == "Applications")) {
            echo "check-square";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 42
($context["this"] ?? null), "page", [], "any", false, false, false, 42), "title", [], "any", false, false, false, 42) == "Groups")) {
            echo "users";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 43
($context["this"] ?? null), "page", [], "any", false, false, false, 43), "title", [], "any", false, false, false, 43) == "Participant Accounts")) {
            echo "user";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 44
($context["this"] ?? null), "page", [], "any", false, false, false, 44), "title", [], "any", false, false, false, 44) == "Communications")) {
            echo "satellite-dish";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 45
($context["this"] ?? null), "page", [], "any", false, false, false, 45), "title", [], "any", false, false, false, 45) == "Matching")) {
            echo "handshake";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 46
($context["this"] ?? null), "page", [], "any", false, false, false, 46), "title", [], "any", false, false, false, 46) == "Surveys")) {
            echo "poll";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 47
($context["this"] ?? null), "page", [], "any", false, false, false, 47), "title", [], "any", false, false, false, 47) == "Transfering Participants")) {
            echo "truck-moving";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 48
($context["this"] ?? null), "page", [], "any", false, false, false, 48), "title", [], "any", false, false, false, 48) == "Accessibility Settings")) {
            echo "universal-access";
        } else {
            // line 49
            echo "question";
        }
        echo "\"></i>
                 </div>
                 <div class=\"col-md-12 makeMid\">
                 <h2>";
        // line 52
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 52), "title", [], "any", false, false, false, 52), "html", null, true);
        echo " </h2>
                 </div>
                </div>
                  
                </div>
                <div class=\"card-body\">
                <!-- Card Body -->
                ";
        // line 59
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("staticPage"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 60
        echo "               
                <p class=\"makeMid\">We found the following topics under ";
        // line 61
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 61), "title", [], "any", false, false, false, 61), "html", null, true);
        echo "</p>
<style>

</style>
   ";
        // line 65
        if (twig_get_attribute($this->env, $this->source, ($context["staticMenu"] ?? null), "menuItems", [], "any", false, false, false, 65)) {
            // line 66
            echo "                <ul class=\"sub-menu-link-headings\">
                    ";
            // line 67
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["staticMenu"] ?? null), "menuItems", [], "any", false, false, false, 67));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 68
                echo "                    ";
                if ((twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, false, 68) == twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 68), "title", [], "any", false, false, false, 68))) {
                    // line 69
                    echo "                        ";
                    if (twig_get_attribute($this->env, $this->source, $context["item"], "items", [], "any", false, false, false, 69)) {
                        // line 70
                        echo "                            ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["item"], "items", [], "any", false, false, false, 70));
                        foreach ($context['_seq'] as $context["_key"] => $context["childItem"]) {
                            // line 71
                            echo "                                <li class=\"";
                            echo ((twig_get_attribute($this->env, $this->source, $context["childItem"], "isActive", [], "any", false, false, false, 71)) ? ("active") : (""));
                            echo "\">
                                    <a href=\"";
                            // line 72
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["childItem"], "url", [], "any", false, false, false, 72), "html", null, true);
                            echo "\">";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["childItem"], "title", [], "any", false, false, false, 72), "html", null, true);
                            echo "</a>
                                </li>
                            ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['childItem'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 75
                        echo "                        ";
                    }
                    // line 76
                    echo "                        ";
                }
                // line 77
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 78
            echo "                </ul>
            ";
        }
        // line 80
        echo "


                ";
        // line 83
        if ((twig_length_filter($this->env, ($context["prev_title"] ?? null)) > 0)) {
            // line 84
            echo "<div class=\"linkButton\"><a href=\"../../../coh";
            echo twig_escape_filter($this->env, ($context["prev_url"] ?? null), "html", null, true);
            echo "\" class=\"previous-link\"><span class=\"makeGrey\">&lt; &lt; Previous topic</span><span class=\"newLine\">";
            echo twig_escape_filter($this->env, ($context["prev_title"] ?? null), "html", null, true);
            echo "</span></a></div>
";
        }
        // line 86
        echo "
";
        // line 87
        if ((twig_length_filter($this->env, ($context["next_title"] ?? null)) > 0)) {
            // line 88
            echo "<div class=\"linkButton\"><a href=\"../../../coh";
            echo twig_escape_filter($this->env, ($context["next_url"] ?? null), "html", null, true);
            echo "\" class=\"next-link\"><span class=\"makeGrey\">Next topic &gt; &gt;</span><span class=\"newLine\">";
            echo twig_escape_filter($this->env, ($context["next_title"] ?? null), "html", null, true);
            echo "</span></a></div>
";
        }
        // line 90
        echo "                </div>
              </div>
            </div>

            
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

     
";
        // line 101
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("foot"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/layouts/topicIndex.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  250 => 101,  237 => 90,  229 => 88,  227 => 87,  224 => 86,  216 => 84,  214 => 83,  209 => 80,  205 => 78,  199 => 77,  196 => 76,  193 => 75,  182 => 72,  177 => 71,  172 => 70,  169 => 69,  166 => 68,  162 => 67,  159 => 66,  157 => 65,  150 => 61,  147 => 60,  143 => 59,  133 => 52,  126 => 49,  122 => 48,  119 => 47,  116 => 46,  113 => 45,  110 => 44,  107 => 43,  104 => 42,  101 => 41,  98 => 40,  95 => 39,  92 => 38,  91 => 37,  69 => 20,  61 => 14,  57 => 13,  49 => 7,  45 => 6,  39 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% partial 'head' %}

  <!-- Page Wrapper -->
  <div id=\"wrapper\">

   {% partial 'sidebar' %}

    <!-- Content Wrapper -->
    <div id=\"content-wrapper\" class=\"d-flex flex-column\">

      <!-- Main Content -->
      <div id=\"content\">
{% partial 'navbar' %}

        <!-- Begin Page Content -->
        <div class=\"container-fluid\">

          <!-- Page Heading 
          <div class=\"d-sm-flex align-items-center justify-content-between mb-4\">
            <h1 class=\"h3 mb-0 text-gray-800 breadcumbs\">{% component 'staticBreadcrumbs' %}</h1>

          </div>-->

          

          <!-- Content Row -->

          <div class=\"row\">

            <!-- Area Chart -->
            <div class=\"col-xl-12 col-lg-12\">
              <div class=\"card shadow mb-4\">
                <!-- Card Header - Dropdown -->
                <div class=\"card-header py-3 d-flex flex-row align-items-center justify-content-between\">
                   <div class=\"row\" style=\"    width: 100%;\">
                <div class=\"col-md-12 makeMid\">
                 <i class=\"fas fa-fw fa-{% if 
                 this.page.title == \"Platform Overview\" %}desktop{% elseif 
                 this.page.title==\"Program Settings\" %}calendar-day{% elseif 
                 this.page.title==\"Resources\" %}file-pdf{% elseif 
                 this.page.title==\"Applications\" %}check-square{% elseif 
                 this.page.title==\"Groups\" %}users{% elseif 
                 this.page.title==\"Participant Accounts\" %}user{% elseif 
                 this.page.title==\"Communications\" %}satellite-dish{% elseif 
                 this.page.title==\"Matching\" %}handshake{% elseif 
                 this.page.title==\"Surveys\" %}poll{% elseif 
                 this.page.title==\"Transfering Participants\" %}truck-moving{% elseif
                 this.page.title==\"Accessibility Settings\" %}universal-access{% else
                 %}question{% endif %}\"></i>
                 </div>
                 <div class=\"col-md-12 makeMid\">
                 <h2>{{ this.page.title }} </h2>
                 </div>
                </div>
                  
                </div>
                <div class=\"card-body\">
                <!-- Card Body -->
                {% component 'staticPage' %}
               
                <p class=\"makeMid\">We found the following topics under {{ this.page.title }}</p>
<style>

</style>
   {% if staticMenu.menuItems %}
                <ul class=\"sub-menu-link-headings\">
                    {% for item in staticMenu.menuItems %}
                    {%if item.title == this.page.title %}
                        {% if item.items %}
                            {% for childItem in item.items %}
                                <li class=\"{{ childItem.isActive ? 'active' : '' }}\">
                                    <a href=\"{{ childItem.url }}\">{{ childItem.title }}</a>
                                </li>
                            {% endfor %}
                        {% endif %}
                        {% endif %}
                    {% endfor %}
                </ul>
            {% endif %}



                {% if prev_title|length > 0 %}
<div class=\"linkButton\"><a href=\"../../../coh{{ prev_url }}\" class=\"previous-link\"><span class=\"makeGrey\">&lt; &lt; Previous topic</span><span class=\"newLine\">{{ prev_title }}</span></a></div>
{% endif%}

{% if next_title|length > 0 %}
<div class=\"linkButton\"><a href=\"../../../coh{{ next_url }}\" class=\"next-link\"><span class=\"makeGrey\">Next topic &gt; &gt;</span><span class=\"newLine\">{{ next_title }}</span></a></div>
{% endif%}
                </div>
              </div>
            </div>

            
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

     
{% partial 'foot' %}", "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/layouts/topicIndex.htm", "");
    }
}
