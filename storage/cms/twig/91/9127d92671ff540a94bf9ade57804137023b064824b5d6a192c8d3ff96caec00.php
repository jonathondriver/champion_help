<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\_xampp\htdocs\coh/themes/art-mentoring-help/partials/menu-items.htm */
class __TwigTemplate_e3947e9c9cda86da9388d0ad415e37a747af6a12e1cfdc4309ed4a4c73846890 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["count"] = 0;
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 3
            $context["count"] = (($context["count"] ?? null) + 1);
            // line 4
            if ((twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, false, 4) != "All Topics")) {
                // line 5
                echo "<li class=\"nav-item\">
        <a class=\"nav-link ";
                // line 6
                echo (((twig_get_attribute($this->env, $this->source, $context["item"], "isActive", [], "any", false, false, false, 6) || twig_get_attribute($this->env, $this->source, $context["item"], "isChildActive", [], "any", false, false, false, 6))) ? ("") : ("collapsed"));
                echo "\" href=\"#\" data-toggle=\"collapse\" data-target=\"#collapse";
                echo twig_escape_filter($this->env, ($context["count"] ?? null), "html", null, true);
                echo "\" aria-expanded=\"";
                echo (((twig_get_attribute($this->env, $this->source, $context["item"], "isActive", [], "any", false, false, false, 6) || twig_get_attribute($this->env, $this->source, $context["item"], "isChildActive", [], "any", false, false, false, 6))) ? ("true") : ("false"));
                echo "\" aria-controls=\"collapse";
                echo twig_escape_filter($this->env, ($context["count"] ?? null), "html", null, true);
                echo "\">
          <i class=\"fas fa-fw fa-";
                // line 7
                if ((($context["count"] ?? null) == 1)) {
                    echo "desktop";
                } elseif ((($context["count"] ?? null) == 2)) {
                    echo "calendar-day";
                } elseif ((($context["count"] ?? null) == 3)) {
                    echo "file-pdf";
                } elseif ((($context["count"] ?? null) == 4)) {
                    echo "check-square";
                } elseif ((($context["count"] ?? null) == 5)) {
                    echo "users";
                } elseif ((($context["count"] ?? null) == 6)) {
                    echo "user";
                } elseif ((($context["count"] ?? null) == 7)) {
                    echo "satellite-dish";
                } elseif ((($context["count"] ?? null) == 8)) {
                    echo "handshake";
                } elseif ((($context["count"] ?? null) == 9)) {
                    echo "poll";
                } elseif ((($context["count"] ?? null) == 10)) {
                    echo "truck-moving";
                } elseif ((($context["count"] ?? null) == 11)) {
                    echo "universal-access";
                }
                echo "\"></i>
          <span>";
                // line 8
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, false, 8), "html", null, true);
                echo "</span>
        </a>
        <div id=\"collapse";
                // line 10
                echo twig_escape_filter($this->env, ($context["count"] ?? null), "html", null, true);
                echo "\" class=\"collapse ";
                echo (((twig_get_attribute($this->env, $this->source, $context["item"], "isActive", [], "any", false, false, false, 10) || twig_get_attribute($this->env, $this->source, $context["item"], "isChildActive", [], "any", false, false, false, 10))) ? ("show") : (""));
                echo "\" aria-labelledby=\"heading";
                echo twig_escape_filter($this->env, ($context["count"] ?? null), "html", null, true);
                echo "\" data-parent=\"#accordionSidebar\" style=\"\">
          <div class=\"bg-white py-2 collapse-inner rounded\">
          ";
                // line 12
                if (twig_get_attribute($this->env, $this->source, $context["item"], "items", [], "any", false, false, false, 12)) {
                    // line 13
                    echo "            <!--<h6 class=\"collapse-header\">Available help:</h6>-->
            
            ";
                    // line 15
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["item"], "items", [], "any", false, false, false, 15));
                    foreach ($context['_seq'] as $context["_key"] => $context["subitem"]) {
                        // line 16
                        echo "            <a class=\"collapse-item  ";
                        echo ((twig_get_attribute($this->env, $this->source, $context["subitem"], "isActive", [], "any", false, false, false, 16)) ? ("active") : (""));
                        echo "\" href=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["subitem"], "url", [], "any", false, false, false, 16), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["subitem"], "title", [], "any", false, false, false, 16), "html", null, true);
                        echo "</a>
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subitem'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 18
                    echo "        ";
                }
                // line 19
                echo "
          </div>
        </div>
      </li>
      ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/partials/menu-items.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 19,  121 => 18,  108 => 16,  104 => 15,  100 => 13,  98 => 12,  89 => 10,  84 => 8,  58 => 7,  48 => 6,  45 => 5,  43 => 4,  41 => 3,  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set count = 0 %}
{% for item in items %}
{% set count = count + 1 %}
{% if item.title != 'All Topics' %}
<li class=\"nav-item\">
        <a class=\"nav-link {{ item.isActive or item.isChildActive ? '' : 'collapsed' }}\" href=\"#\" data-toggle=\"collapse\" data-target=\"#collapse{{ count }}\" aria-expanded=\"{{ item.isActive or item.isChildActive ? 'true' : 'false' }}\" aria-controls=\"collapse{{ count }}\">
          <i class=\"fas fa-fw fa-{% if count == 1 %}desktop{% elseif count==2 %}calendar-day{% elseif count==3 %}file-pdf{% elseif count==4 %}check-square{% elseif count==5 %}users{% elseif count==6 %}user{% elseif count==7 %}satellite-dish{% elseif count==8 %}handshake{% elseif count==9 %}poll{% elseif count==10 %}truck-moving{% elseif count==11 %}universal-access{% endif %}\"></i>
          <span>{{ item.title }}</span>
        </a>
        <div id=\"collapse{{ count }}\" class=\"collapse {{ item.isActive or item.isChildActive ? 'show' : '' }}\" aria-labelledby=\"heading{{ count }}\" data-parent=\"#accordionSidebar\" style=\"\">
          <div class=\"bg-white py-2 collapse-inner rounded\">
          {% if item.items %}
            <!--<h6 class=\"collapse-header\">Available help:</h6>-->
            
            {% for subitem in item.items %}
            <a class=\"collapse-item  {{ subitem.isActive  ?  'active' : ''}}\" href=\"{{ subitem.url }}\">{{ subitem.title }}</a>
            {% endfor %}
        {% endif %}

          </div>
        </div>
      </li>
      {% endif %}
{% endfor %}", "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/partials/menu-items.htm", "");
    }
}
