<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\_xampp\htdocs\coh/themes/art-mentoring-help/layouts/home.htm */
class __TwigTemplate_26da42023919f90434cb0e36db583745a6e4fa687b2e2fff667434a3afb7f2ee extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("head"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 2
        echo "
  <!-- Page Wrapper -->
  <div id=\"wrapper\">


   
    <!-- Content Wrapper -->
    <div id=\"content-wrapper\" class=\"d-flex flex-column\">

      <!-- Main Content -->
      <div id=\"content\">

       ";
        // line 14
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("navbar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 15
        echo "
        <!-- Begin Page Content -->
        <div class=\"container-fluid\">
        <!-- ----------------------------------------------------------->
  
  <link href=\"../../../coh/themes/art-mentoring-help/assets/vendor/fontawesome-free/css/all.css\" rel=\"stylesheet\" type=\"text/css\">
  <link href=\"https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i\" rel=\"stylesheet\">

  <!-- Custom styles for this template-->
  <link href=\"../../../coh/themes/art-mentoring-help/assets/css/sb-admin-2.css\" rel=\"stylesheet\">
<link href=\"../../../coh/themes/art-mentoring-help/assets/aom.css\" rel=\"stylesheet\" type=\"text/css\">
\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"../../../coh/themes/art-mentoring-help/assets/css/default.css\" />
\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"../../../coh/themes/art-mentoring-help/assets/css/component.css\" />
\t\t<script src=\"../../../coh/themes/art-mentoring-help/assets/js/modernizr.custom.js\"></script>
\t\t<style>
\t\t.cbp-ig-grid .fa,.cbp-ig-grid .fas {
\t\t    padding-top: 30px;
padding-bottom: 50px;
    font-size: 100px;
}
.cbp-ig-grid li{
    height: 350px!important;
}
.cbp-ig-grid li > a:hover .cbp-ig-icon {
\t-webkit-transform: translateY(10px);
\t-moz-transform: translateY(10px);
\t-ms-transform: translateY(10px);
\ttransform: translateY(10px);
}

.cbp-ig-grid li > a:hover .cbp-ig-title ,
.cbp-ig-grid li > a:hover .fa-fw {
\tcolor: #fff!important;
}
.main {
    width: 100%!important; 
}
\t\t</style>
\t</head>
\t<body>
\t
\t\t<div class=\"container\">
\t\t\t<header class=\"clearfix\">
\t\t\t\t<span><img src=\"storage/app/media/AoM-bullet.png\" style=\"max-width: 94px;
    margin-right: 40px;\" />Art of Mentoring - Champion Help Topics</span>
\t\t\t\t
\t\t\t</header>\t
\t\t\t<div class=\"main\">
\t\t\t\t<ul class=\"cbp-ig-grid\">
\t\t\t\t";
        // line 64
        $context["count"] = 0;
        // line 65
        echo "\t\t\t
                ";
        // line 66
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["staticMenu2"] ?? null), "menuItems", [], "any", false, false, false, 66));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 67
            echo "                    ";
            $context["count"] = (($context["count"] ?? null) + 1);
            // line 68
            echo "\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"";
            // line 69
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "url", [], "any", false, false, false, 69), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t<i class=\"fas fa-fw fa-";
            // line 70
            if ((($context["count"] ?? null) == 1)) {
                echo "desktop";
            } elseif ((($context["count"] ?? null) == 2)) {
                echo "calendar-day";
            } elseif ((($context["count"] ?? null) == 3)) {
                echo "file-pdf";
            } elseif ((($context["count"] ?? null) == 4)) {
                echo "check-square";
            } elseif ((($context["count"] ?? null) == 5)) {
                echo "users";
            } elseif ((($context["count"] ?? null) == 6)) {
                echo "user";
            } elseif ((($context["count"] ?? null) == 7)) {
                echo "satellite-dish";
            } elseif ((($context["count"] ?? null) == 8)) {
                echo "handshake";
            } elseif ((($context["count"] ?? null) == 9)) {
                echo "poll";
            } elseif ((($context["count"] ?? null) == 10)) {
                echo "truck-moving";
            } elseif ((($context["count"] ?? null) == 11)) {
                echo "universal-access";
            } else {
                echo "question-circle";
            }
            echo "\"></i>
\t\t\t\t\t\t\t<h3 class=\"cbp-ig-title\">";
            // line 71
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, false, 71), "html", null, true);
            echo "</h3>
\t\t\t\t\t\t\t<span class=\"cbp-ig-category\">Read more..</span>
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "\t\t\t
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
                </div>
              </div>
            </div>

            
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      

";
        // line 92
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("foot"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/layouts/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 92,  166 => 76,  155 => 71,  127 => 70,  123 => 69,  120 => 68,  117 => 67,  113 => 66,  110 => 65,  108 => 64,  57 => 15,  53 => 14,  39 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% partial 'head' %}

  <!-- Page Wrapper -->
  <div id=\"wrapper\">


   
    <!-- Content Wrapper -->
    <div id=\"content-wrapper\" class=\"d-flex flex-column\">

      <!-- Main Content -->
      <div id=\"content\">

       {% partial 'navbar' %}

        <!-- Begin Page Content -->
        <div class=\"container-fluid\">
        <!-- ----------------------------------------------------------->
  
  <link href=\"../../../coh/themes/art-mentoring-help/assets/vendor/fontawesome-free/css/all.css\" rel=\"stylesheet\" type=\"text/css\">
  <link href=\"https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i\" rel=\"stylesheet\">

  <!-- Custom styles for this template-->
  <link href=\"../../../coh/themes/art-mentoring-help/assets/css/sb-admin-2.css\" rel=\"stylesheet\">
<link href=\"../../../coh/themes/art-mentoring-help/assets/aom.css\" rel=\"stylesheet\" type=\"text/css\">
\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"../../../coh/themes/art-mentoring-help/assets/css/default.css\" />
\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"../../../coh/themes/art-mentoring-help/assets/css/component.css\" />
\t\t<script src=\"../../../coh/themes/art-mentoring-help/assets/js/modernizr.custom.js\"></script>
\t\t<style>
\t\t.cbp-ig-grid .fa,.cbp-ig-grid .fas {
\t\t    padding-top: 30px;
padding-bottom: 50px;
    font-size: 100px;
}
.cbp-ig-grid li{
    height: 350px!important;
}
.cbp-ig-grid li > a:hover .cbp-ig-icon {
\t-webkit-transform: translateY(10px);
\t-moz-transform: translateY(10px);
\t-ms-transform: translateY(10px);
\ttransform: translateY(10px);
}

.cbp-ig-grid li > a:hover .cbp-ig-title ,
.cbp-ig-grid li > a:hover .fa-fw {
\tcolor: #fff!important;
}
.main {
    width: 100%!important; 
}
\t\t</style>
\t</head>
\t<body>
\t
\t\t<div class=\"container\">
\t\t\t<header class=\"clearfix\">
\t\t\t\t<span><img src=\"storage/app/media/AoM-bullet.png\" style=\"max-width: 94px;
    margin-right: 40px;\" />Art of Mentoring - Champion Help Topics</span>
\t\t\t\t
\t\t\t</header>\t
\t\t\t<div class=\"main\">
\t\t\t\t<ul class=\"cbp-ig-grid\">
\t\t\t\t{% set count = 0 %}
\t\t\t
                {% for item in staticMenu2.menuItems %}
                    {% set count = count + 1 %}
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"{{ item.url }}\">
\t\t\t\t\t\t\t<i class=\"fas fa-fw fa-{% if count == 1 %}desktop{% elseif count==2 %}calendar-day{% elseif count==3 %}file-pdf{% elseif count==4 %}check-square{% elseif count==5 %}users{% elseif count==6 %}user{% elseif count==7 %}satellite-dish{% elseif count==8 %}handshake{% elseif count==9 %}poll{% elseif count==10 %}truck-moving{% elseif count==11 %}universal-access{% else %}question-circle{% endif %}\"></i>
\t\t\t\t\t\t\t<h3 class=\"cbp-ig-title\">{{ item.title }}</h3>
\t\t\t\t\t\t\t<span class=\"cbp-ig-category\">Read more..</span>
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t{% endfor %}
\t\t\t
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
                </div>
              </div>
            </div>

            
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      

{% partial 'foot' %}", "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/layouts/home.htm", "");
    }
}
