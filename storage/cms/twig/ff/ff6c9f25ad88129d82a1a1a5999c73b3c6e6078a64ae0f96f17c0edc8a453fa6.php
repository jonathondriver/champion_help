<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\_xampp\htdocs\coh/themes/art-mentoring-help/partials/head.htm */
class __TwigTemplate_9d05db14a2d3df417778e636803649ac69bdb7fc1494909d1f5e8a8525f684b3 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>

  <meta charset=\"utf-8\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
  <meta name=\"description\" content=\"\">
  <meta name=\"author\" content=\"Art of Mentoring\">
        <title>Art of Mentoring - ";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 11), "title", [], "any", false, false, false, 11), "html", null, true);
        echo "</title>
        <meta name=\"description\" content=\"";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 12), "meta_description", [], "any", false, false, false, 12), "html", null, true);
        echo "\">
        <meta name=\"title\" content=\"";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 13), "meta_title", [], "any", false, false, false, 13), "html", null, true);
        echo "\">
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 14
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/october.png");
        echo "\" />

  <!-- Custom fonts for this template-->
  
  <link href=\"../../../coh/themes/art-mentoring-help/assets/vendor/fontawesome-free/css/all.css\" rel=\"stylesheet\" type=\"text/css\">
  <link href=\"https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i\" rel=\"stylesheet\">
  <link href=\"https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap\" rel=\"stylesheet\">
  <!-- Custom styles for this template-->
  <link href=\"../../../coh/themes/art-mentoring-help/assets/css/sb-admin-2.css\" rel=\"stylesheet\">
<link href=\"../../../coh/themes/art-mentoring-help/assets/aom.css\" rel=\"stylesheet\" type=\"text/css\">
</head>

<body id=\"page-top\">";
    }

    public function getTemplateName()
    {
        return "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/partials/head.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 14,  55 => 13,  51 => 12,  47 => 11,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">

<head>

  <meta charset=\"utf-8\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
  <meta name=\"description\" content=\"\">
  <meta name=\"author\" content=\"Art of Mentoring\">
        <title>Art of Mentoring - {{ this.page.title }}</title>
        <meta name=\"description\" content=\"{{ this.page.meta_description }}\">
        <meta name=\"title\" content=\"{{ this.page.meta_title }}\">
        <link rel=\"icon\" type=\"image/png\" href=\"{{ 'assets/images/october.png'|theme }}\" />

  <!-- Custom fonts for this template-->
  
  <link href=\"../../../coh/themes/art-mentoring-help/assets/vendor/fontawesome-free/css/all.css\" rel=\"stylesheet\" type=\"text/css\">
  <link href=\"https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i\" rel=\"stylesheet\">
  <link href=\"https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap\" rel=\"stylesheet\">
  <!-- Custom styles for this template-->
  <link href=\"../../../coh/themes/art-mentoring-help/assets/css/sb-admin-2.css\" rel=\"stylesheet\">
<link href=\"../../../coh/themes/art-mentoring-help/assets/aom.css\" rel=\"stylesheet\" type=\"text/css\">
</head>

<body id=\"page-top\">", "C:\\_xampp\\htdocs\\coh/themes/art-mentoring-help/partials/head.htm", "");
    }
}
