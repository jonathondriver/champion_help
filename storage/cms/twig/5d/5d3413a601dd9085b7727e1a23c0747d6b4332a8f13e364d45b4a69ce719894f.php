<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\_xampp\htdocs\coh/plugins/offline/sitesearch/components/searchinput/default.htm */
class __TwigTemplate_10acaff7fc8742cfe386fd640b19a46947ca79e775ccf6f7c06fa240c1ca0f08 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<form class=\"form-inline mr-auto w-100 navbar-search\" action=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "searchPage", [], "any", false, false, false, 1));
        echo "?q=";
        echo twig_escape_filter($this->env, twig_urlencode_filter(($context["query"] ?? null), true), "html", null, true);
        echo "\" method=\"get\">
    <div class=\"ss-search-form input-group\">
        <div class=\"ss-search-form__input\">
            <input name=\"q\" class=\"form-control bg-light border-0 small\" type=\"text\" placeholder=\"What are you looking for?\" 
                   value=\"";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "query", [], "any", false, false, false, 5), "html", null, true);
        echo "\"
                   autocomplete=\"off\"
                   autofocus
                    ";
        // line 8
        if (twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "useAutoComplete", [], "any", false, false, false, 8)) {
            // line 9
            echo "                        data-track-input
                        data-request=\"";
            // line 10
            echo twig_escape_filter($this->env, ($context["__SELF__"] ?? null), "html", null, true);
            echo "::onType\"
                        data-request-before-update=\"document.getElementById('autocomplete-results').classList.add('ss-search-form__results--visible')\"
                        data-request-update=\"'";
            // line 12
            echo twig_escape_filter($this->env, ($context["__SELF__"] ?? null), "html", null, true);
            echo "::autocomplete': '#autocomplete-results'\"
                    ";
        }
        // line 14
        echo "            ><div class=\"input-group-append\" style=\"display: inline-block;\">
            <button class=\"btn btn-primary ss-search-form__submit\" type=\"submit\"><i class=\"fas fa-search fa-sm\"></i></button>
            </div>
        </div>
        <div class=\"ss-search-form__results\" id=\"autocomplete-results\"></div>
    </div>
</form>
";
    }

    public function getTemplateName()
    {
        return "C:\\_xampp\\htdocs\\coh/plugins/offline/sitesearch/components/searchinput/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 14,  61 => 12,  56 => 10,  53 => 9,  51 => 8,  45 => 5,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<form class=\"form-inline mr-auto w-100 navbar-search\" action=\"{{ __SELF__.searchPage | page }}?q={{ query | url_encode(true) }}\" method=\"get\">
    <div class=\"ss-search-form input-group\">
        <div class=\"ss-search-form__input\">
            <input name=\"q\" class=\"form-control bg-light border-0 small\" type=\"text\" placeholder=\"What are you looking for?\" 
                   value=\"{{ __SELF__.query }}\"
                   autocomplete=\"off\"
                   autofocus
                    {% if __SELF__.useAutoComplete %}
                        data-track-input
                        data-request=\"{{ __SELF__ }}::onType\"
                        data-request-before-update=\"document.getElementById('autocomplete-results').classList.add('ss-search-form__results--visible')\"
                        data-request-update=\"'{{ __SELF__ }}::autocomplete': '#autocomplete-results'\"
                    {% endif %}
            ><div class=\"input-group-append\" style=\"display: inline-block;\">
            <button class=\"btn btn-primary ss-search-form__submit\" type=\"submit\"><i class=\"fas fa-search fa-sm\"></i></button>
            </div>
        </div>
        <div class=\"ss-search-form__results\" id=\"autocomplete-results\"></div>
    </div>
</form>
", "C:\\_xampp\\htdocs\\coh/plugins/offline/sitesearch/components/searchinput/default.htm", "");
    }
}
