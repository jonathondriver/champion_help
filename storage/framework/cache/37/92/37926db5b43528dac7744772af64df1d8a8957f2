1565740111a:1:{s:54:"transfering-participants-transferring-participants.htm";a:7:{s:8:"fileName";s:54:"transfering-participants-transferring-participants.htm";s:7:"content";s:7941:"[viewBag]
title = "Transferring Participants"
url = "/transfering-participants/transferring-participants"
layout = "defaultWithSideBar"
is_hidden = 0
navigation_hidden = 0
==
<h2>Adding participants from old programs</h2>

<p>When you finish one program and start a new one, your CSM (Client Success Manager) will arrange to duplicate your program stream and copy across relevant information. This gives you a clean slate to start your next mentoring program, without old sent communications and surveys results cluttering the interface.</p>

<p>If you have Participants, usually Mentors, who have participated before and who have opted to join the new program, we can save them the time of completing a new profile by transferring their data from one program to another.</p>

<p>It is advisable that mentees start the application process again, rather than be transferred, as they will probably have new goals and a new focus.</p>

<h2>How to move Participants to the new program</h2>

<h4>Process Option 1: Duplicate program and then transfer mentors</h4>

<p>This option is useful when:</p>

<p>A.There are Mentors who have participated in more than one past program, that would like to join the new program; or</p>

<p>B. There is a known small group of past Participants to transfer, with their application data.</p>

<p>There are five steps:</p>

<p><strong>1. Select which program you wish to duplicate</strong></p>

<p>Discuss this with your CSM – it may be the last program or it may be another.</p>

<p><strong>2. Identify Participants who wish to participate in the second program</strong></p>

<p>To do this, you should go to the Champion portal of the program in which the Participant already has an Account and:</p>

<ol>
	<li>Send a message to all or selected Participants, asking them to indicate their willingness to:

		<ol type="i">
			<li>mentor again,</li>
			<li>switch from mentee to mentor for the second program, or</li>
			<li>re-enter the program as a mentee (if you wish to allow that).</li>
		</ol>
	</li>
	<li>Set up one Custom Group called something like “Mentors to transfer from 2018 Program”;</li>
	<li>Manually add Participants to this Group once they have agreed to be added to the new program.</li>
</ol>

<p>If you have Mentors to transfer from more than one program, you will need to repeat this step for each program.</p>

<p>Note: In order to receive messages from the old program, each participant must still be Active (not suspended) in that program.</p>

<p><strong>3. Contact your CSM to request transfer</strong></p>

<p>Now that you have them in a Custom Group, the CSM will arrange for the entire group to be added to the new program. Any Participants who were both Mentor and Mentee will have their Mentee data deleted, so they enter the program only as a Mentor. They can contact you and ask you to add the Mentee role for them if required. No messages are triggered to be sent to them when added this way.</p>

<p>When you view Groups in the new program, you will see the Group that has been transferred, so you can identify past Participants in the new program.</p>

<p><strong>4. Check in the new program that the transfer has been successfully made</strong></p>

<p>Once AoM has duplicated the data and transferred the Mentor profiles, we will ask you to check for accuracy.</p>

<p><strong>5. Manage your instructions to Applicants</strong></p>

<ol>
	<li>Tell your first-time Mentors and Mentees to select the “Apply” button to join this program</li>
	<li>Tell Mentors that have been transferred to the new program to log in using the Returning Participant or Login button and their previous username and password.</li>
	<li>Tell past Mentors or Mentees who have not been transferred, to either:

		<ol type="i">
			<li>Request that you transfer their data to this program and then they can log in; or</li>
			<li>Apply again using the “Apply” button – in which case they will complete the application from scratch. (If they try to log in using their old account details to a program in which they are not registered, they will not be recognized and will get an error message).</li>
		</ol>
	</li>
</ol>
<hr>

<h4>Process Option 2: Duplicate program and mentors at the same time</h4>

<p>This option is useful when:</p>

<ol>
	<li>You want to transfer Mentors from only one past program;</li>
	<li>You believe that the majority of past Mentors will mentor again, so, rather than add a small number of repeats, you will suspend or later delete by exception those who decline the invitation.</li>
</ol>

<p>There are five steps:</p>

<p><strong>1. Select which program you wish to duplicate</strong></p>

<p>Discuss this with your CSM – it may be the last program or it may be another.</p>

<p><strong>2. We will duplicate the program and the Mentors from this program</strong></p>

<p>All Mentors will appear in the new program. Any Participants who were both Mentor and Mentee will have their Mentee data deleted, so they enter the program only as a Mentor. They can contact you and ask you to add the Mentee role for them if required. On transfer, an invitation is sent from the new program to the Mentor, to apply to this new program. They can do so using either the “Apply” or “Returning Participant”/ “Log In” buttons (see next page).</p>

<p><img src="../../../coh/storage/app/media/tra1.jpg" class="fr-fic fr-dii"></p>

<p><strong>3. Check in the new program that the transfer has been successfully made</strong></p>

<p>Once AoM has duplicated the data and transferred the Mentor profiles, we will ask you to check for accuracy. All transferred Participants will have the status “Inactive Pending” until they log in to this program to activate their registration, and then they will change status to “Active Unmatch” until matched. Apart from the invitation to join the program, they can receive no messages from this program whilst inactive.</p>

<p><strong>4. Create a Custom Group with transferred Participants</strong></p>

<p>You will want to identify those Mentors who have been transferred from a past program. To do this:</p>

<ol>
	<li>Set up one Custom Group called something like “Mentors transferred from 2018 Program”;</li>
	<li>Manually add the transferred Participants to this Group.</li>
</ol>

<p><strong>5. Manage your instructions to Applicants</strong></p>

<ol>
	<li>Tell your first-time Mentors and Mentees, as well as return Mentees to select the “Apply” button to join this program and complete their application from scratch.</li>
	<li>Tell Mentors that have been transferred to the new program to log in using the Returning Participant or Login button. Their profiles may just need updating if you have modified the application questions.</li>
	<li>Mentors who do not respond to the invitation in Step 2 cannot receive messages from this program. If you wish to ask them again to consider joining, you will need to send a message from the old program.</li>
</ol>

<p><strong>6. Suspend or delete transferred Participants who have not activated their account</strong></p>

<p>You will want to remove those Participants that have not accepted the invitation to join the program, once applications are closed and you have completed matching.</p>

<p>NOTE: In the duplication process, Mentor application data is retained from one program to another. This is only the case where an application form remains unchanged from one program to another. If new questions are added, duplicated mentors will obviously need to re-complete these questions. Generally it will be a good idea to direct them to check over their application regardless, as personal circumstances will often change from year to year. Personal information such as profile pictures, phone numbers etc. are also retained.</p>";s:5:"mtime";i:1556062527;s:6:"markup";s:7759:"<h2>Adding participants from old programs</h2>

<p>When you finish one program and start a new one, your CSM (Client Success Manager) will arrange to duplicate your program stream and copy across relevant information. This gives you a clean slate to start your next mentoring program, without old sent communications and surveys results cluttering the interface.</p>

<p>If you have Participants, usually Mentors, who have participated before and who have opted to join the new program, we can save them the time of completing a new profile by transferring their data from one program to another.</p>

<p>It is advisable that mentees start the application process again, rather than be transferred, as they will probably have new goals and a new focus.</p>

<h2>How to move Participants to the new program</h2>

<h4>Process Option 1: Duplicate program and then transfer mentors</h4>

<p>This option is useful when:</p>

<p>A.There are Mentors who have participated in more than one past program, that would like to join the new program; or</p>

<p>B. There is a known small group of past Participants to transfer, with their application data.</p>

<p>There are five steps:</p>

<p><strong>1. Select which program you wish to duplicate</strong></p>

<p>Discuss this with your CSM – it may be the last program or it may be another.</p>

<p><strong>2. Identify Participants who wish to participate in the second program</strong></p>

<p>To do this, you should go to the Champion portal of the program in which the Participant already has an Account and:</p>

<ol>
	<li>Send a message to all or selected Participants, asking them to indicate their willingness to:

		<ol type="i">
			<li>mentor again,</li>
			<li>switch from mentee to mentor for the second program, or</li>
			<li>re-enter the program as a mentee (if you wish to allow that).</li>
		</ol>
	</li>
	<li>Set up one Custom Group called something like “Mentors to transfer from 2018 Program”;</li>
	<li>Manually add Participants to this Group once they have agreed to be added to the new program.</li>
</ol>

<p>If you have Mentors to transfer from more than one program, you will need to repeat this step for each program.</p>

<p>Note: In order to receive messages from the old program, each participant must still be Active (not suspended) in that program.</p>

<p><strong>3. Contact your CSM to request transfer</strong></p>

<p>Now that you have them in a Custom Group, the CSM will arrange for the entire group to be added to the new program. Any Participants who were both Mentor and Mentee will have their Mentee data deleted, so they enter the program only as a Mentor. They can contact you and ask you to add the Mentee role for them if required. No messages are triggered to be sent to them when added this way.</p>

<p>When you view Groups in the new program, you will see the Group that has been transferred, so you can identify past Participants in the new program.</p>

<p><strong>4. Check in the new program that the transfer has been successfully made</strong></p>

<p>Once AoM has duplicated the data and transferred the Mentor profiles, we will ask you to check for accuracy.</p>

<p><strong>5. Manage your instructions to Applicants</strong></p>

<ol>
	<li>Tell your first-time Mentors and Mentees to select the “Apply” button to join this program</li>
	<li>Tell Mentors that have been transferred to the new program to log in using the Returning Participant or Login button and their previous username and password.</li>
	<li>Tell past Mentors or Mentees who have not been transferred, to either:

		<ol type="i">
			<li>Request that you transfer their data to this program and then they can log in; or</li>
			<li>Apply again using the “Apply” button – in which case they will complete the application from scratch. (If they try to log in using their old account details to a program in which they are not registered, they will not be recognized and will get an error message).</li>
		</ol>
	</li>
</ol>
<hr>

<h4>Process Option 2: Duplicate program and mentors at the same time</h4>

<p>This option is useful when:</p>

<ol>
	<li>You want to transfer Mentors from only one past program;</li>
	<li>You believe that the majority of past Mentors will mentor again, so, rather than add a small number of repeats, you will suspend or later delete by exception those who decline the invitation.</li>
</ol>

<p>There are five steps:</p>

<p><strong>1. Select which program you wish to duplicate</strong></p>

<p>Discuss this with your CSM – it may be the last program or it may be another.</p>

<p><strong>2. We will duplicate the program and the Mentors from this program</strong></p>

<p>All Mentors will appear in the new program. Any Participants who were both Mentor and Mentee will have their Mentee data deleted, so they enter the program only as a Mentor. They can contact you and ask you to add the Mentee role for them if required. On transfer, an invitation is sent from the new program to the Mentor, to apply to this new program. They can do so using either the “Apply” or “Returning Participant”/ “Log In” buttons (see next page).</p>

<p><img src="../../../coh/storage/app/media/tra1.jpg" class="fr-fic fr-dii"></p>

<p><strong>3. Check in the new program that the transfer has been successfully made</strong></p>

<p>Once AoM has duplicated the data and transferred the Mentor profiles, we will ask you to check for accuracy. All transferred Participants will have the status “Inactive Pending” until they log in to this program to activate their registration, and then they will change status to “Active Unmatch” until matched. Apart from the invitation to join the program, they can receive no messages from this program whilst inactive.</p>

<p><strong>4. Create a Custom Group with transferred Participants</strong></p>

<p>You will want to identify those Mentors who have been transferred from a past program. To do this:</p>

<ol>
	<li>Set up one Custom Group called something like “Mentors transferred from 2018 Program”;</li>
	<li>Manually add the transferred Participants to this Group.</li>
</ol>

<p><strong>5. Manage your instructions to Applicants</strong></p>

<ol>
	<li>Tell your first-time Mentors and Mentees, as well as return Mentees to select the “Apply” button to join this program and complete their application from scratch.</li>
	<li>Tell Mentors that have been transferred to the new program to log in using the Returning Participant or Login button. Their profiles may just need updating if you have modified the application questions.</li>
	<li>Mentors who do not respond to the invitation in Step 2 cannot receive messages from this program. If you wish to ask them again to consider joining, you will need to send a message from the old program.</li>
</ol>

<p><strong>6. Suspend or delete transferred Participants who have not activated their account</strong></p>

<p>You will want to remove those Participants that have not accepted the invitation to join the program, once applications are closed and you have completed matching.</p>

<p>NOTE: In the duplication process, Mentor application data is retained from one program to another. This is only the case where an application form remains unchanged from one program to another. If new questions are added, duplicated mentors will obviously need to re-complete these questions. Generally it will be a good idea to direct them to check over their application regardless, as personal circumstances will often change from year to year. Personal information such as profile pictures, phone numbers etc. are also retained.</p>";s:4:"code";N;s:7:"viewBag";a:5:{s:5:"title";s:25:"Transferring Participants";s:3:"url";s:51:"/transfering-participants/transferring-participants";s:6:"layout";s:18:"defaultWithSideBar";s:9:"is_hidden";s:1:"0";s:17:"navigation_hidden";s:1:"0";}s:12:"parsedMarkup";s:7759:"<h2>Adding participants from old programs</h2>

<p>When you finish one program and start a new one, your CSM (Client Success Manager) will arrange to duplicate your program stream and copy across relevant information. This gives you a clean slate to start your next mentoring program, without old sent communications and surveys results cluttering the interface.</p>

<p>If you have Participants, usually Mentors, who have participated before and who have opted to join the new program, we can save them the time of completing a new profile by transferring their data from one program to another.</p>

<p>It is advisable that mentees start the application process again, rather than be transferred, as they will probably have new goals and a new focus.</p>

<h2>How to move Participants to the new program</h2>

<h4>Process Option 1: Duplicate program and then transfer mentors</h4>

<p>This option is useful when:</p>

<p>A.There are Mentors who have participated in more than one past program, that would like to join the new program; or</p>

<p>B. There is a known small group of past Participants to transfer, with their application data.</p>

<p>There are five steps:</p>

<p><strong>1. Select which program you wish to duplicate</strong></p>

<p>Discuss this with your CSM – it may be the last program or it may be another.</p>

<p><strong>2. Identify Participants who wish to participate in the second program</strong></p>

<p>To do this, you should go to the Champion portal of the program in which the Participant already has an Account and:</p>

<ol>
	<li>Send a message to all or selected Participants, asking them to indicate their willingness to:

		<ol type="i">
			<li>mentor again,</li>
			<li>switch from mentee to mentor for the second program, or</li>
			<li>re-enter the program as a mentee (if you wish to allow that).</li>
		</ol>
	</li>
	<li>Set up one Custom Group called something like “Mentors to transfer from 2018 Program”;</li>
	<li>Manually add Participants to this Group once they have agreed to be added to the new program.</li>
</ol>

<p>If you have Mentors to transfer from more than one program, you will need to repeat this step for each program.</p>

<p>Note: In order to receive messages from the old program, each participant must still be Active (not suspended) in that program.</p>

<p><strong>3. Contact your CSM to request transfer</strong></p>

<p>Now that you have them in a Custom Group, the CSM will arrange for the entire group to be added to the new program. Any Participants who were both Mentor and Mentee will have their Mentee data deleted, so they enter the program only as a Mentor. They can contact you and ask you to add the Mentee role for them if required. No messages are triggered to be sent to them when added this way.</p>

<p>When you view Groups in the new program, you will see the Group that has been transferred, so you can identify past Participants in the new program.</p>

<p><strong>4. Check in the new program that the transfer has been successfully made</strong></p>

<p>Once AoM has duplicated the data and transferred the Mentor profiles, we will ask you to check for accuracy.</p>

<p><strong>5. Manage your instructions to Applicants</strong></p>

<ol>
	<li>Tell your first-time Mentors and Mentees to select the “Apply” button to join this program</li>
	<li>Tell Mentors that have been transferred to the new program to log in using the Returning Participant or Login button and their previous username and password.</li>
	<li>Tell past Mentors or Mentees who have not been transferred, to either:

		<ol type="i">
			<li>Request that you transfer their data to this program and then they can log in; or</li>
			<li>Apply again using the “Apply” button – in which case they will complete the application from scratch. (If they try to log in using their old account details to a program in which they are not registered, they will not be recognized and will get an error message).</li>
		</ol>
	</li>
</ol>
<hr>

<h4>Process Option 2: Duplicate program and mentors at the same time</h4>

<p>This option is useful when:</p>

<ol>
	<li>You want to transfer Mentors from only one past program;</li>
	<li>You believe that the majority of past Mentors will mentor again, so, rather than add a small number of repeats, you will suspend or later delete by exception those who decline the invitation.</li>
</ol>

<p>There are five steps:</p>

<p><strong>1. Select which program you wish to duplicate</strong></p>

<p>Discuss this with your CSM – it may be the last program or it may be another.</p>

<p><strong>2. We will duplicate the program and the Mentors from this program</strong></p>

<p>All Mentors will appear in the new program. Any Participants who were both Mentor and Mentee will have their Mentee data deleted, so they enter the program only as a Mentor. They can contact you and ask you to add the Mentee role for them if required. On transfer, an invitation is sent from the new program to the Mentor, to apply to this new program. They can do so using either the “Apply” or “Returning Participant”/ “Log In” buttons (see next page).</p>

<p><img src="../../../coh/storage/app/media/tra1.jpg" class="fr-fic fr-dii"></p>

<p><strong>3. Check in the new program that the transfer has been successfully made</strong></p>

<p>Once AoM has duplicated the data and transferred the Mentor profiles, we will ask you to check for accuracy. All transferred Participants will have the status “Inactive Pending” until they log in to this program to activate their registration, and then they will change status to “Active Unmatch” until matched. Apart from the invitation to join the program, they can receive no messages from this program whilst inactive.</p>

<p><strong>4. Create a Custom Group with transferred Participants</strong></p>

<p>You will want to identify those Mentors who have been transferred from a past program. To do this:</p>

<ol>
	<li>Set up one Custom Group called something like “Mentors transferred from 2018 Program”;</li>
	<li>Manually add the transferred Participants to this Group.</li>
</ol>

<p><strong>5. Manage your instructions to Applicants</strong></p>

<ol>
	<li>Tell your first-time Mentors and Mentees, as well as return Mentees to select the “Apply” button to join this program and complete their application from scratch.</li>
	<li>Tell Mentors that have been transferred to the new program to log in using the Returning Participant or Login button. Their profiles may just need updating if you have modified the application questions.</li>
	<li>Mentors who do not respond to the invitation in Step 2 cannot receive messages from this program. If you wish to ask them again to consider joining, you will need to send a message from the old program.</li>
</ol>

<p><strong>6. Suspend or delete transferred Participants who have not activated their account</strong></p>

<p>You will want to remove those Participants that have not accepted the invitation to join the program, once applications are closed and you have completed matching.</p>

<p>NOTE: In the duplication process, Mentor application data is retained from one program to another. This is only the case where an application form remains unchanged from one program to another. If new questions are added, duplicated mentors will obviously need to re-complete these questions. Generally it will be a good idea to direct them to check over their application regardless, as personal circumstances will often change from year to year. Personal information such as profile pictures, phone numbers etc. are also retained.</p>";}}